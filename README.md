# EverTrust asciidoc

This container bundles an Asciidoctor distribution for typesetting corporate EverTrust documents.

## Usage

In the directory where your Asciidoc assets live, run:
```
docker run -v $(pwd):/data registry.gitlab.com/evertrust/asciidoc <file.adoc>
```

## What's included

- The default EverTrust theme, including assets (logos and fonts)
- `rouge`, for syntax highlighting in code blocks
- `text-hyphen`, to hyphenate text. To enable this feature, add `:hyphens: fr` to your document header.
- `asciidoctor-diagram` with D2 support to compile inline diagrams

## What's not

You're responsible for setting the `imagesdir` setting in the document header, which will be used to generate diagrams.