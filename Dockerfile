FROM alpine:3.19

RUN apk add --no-cache ruby graphicsmagick

RUN apk add --no-cache graphicsmagick-dev ruby-dev build-base && \
    gem install asciidoctor-pdf prawn-gmagick text-hyphen rouge asciidoctor-kroki && \
    apk del ruby-dev build-base

COPY theme /var/theme

WORKDIR /data

ENTRYPOINT ["asciidoctor-pdf", \
            "-r", "asciidoctor-kroki", \
            "-a", "pdf-theme=/var/theme/default-theme.yml", \
            "-a", "pdf-fontsdir=/var/theme,GEM_FONTS_DIR", \
            "-a", "icons=font", \
            "-a", "source-highlighter=rouge", \
            "-a", "title-logo-image=image:/var/theme/images/evertrust.png[width=400,align=center]", \
            "-a", "rouge-style=github" \
]
